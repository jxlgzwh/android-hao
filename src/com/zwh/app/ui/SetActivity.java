package com.zwh.app.ui;


import java.io.File;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.zwh.android_hao.R;
import com.zwh.app.common.FileUtils;
import com.zwh.app.common.MethodsCompat;
import com.zwh.app.common.UIHelper;
import com.zwh.app.manager.AppConfig;
import com.zwh.app.manager.AppContext;
import com.zwh.app.manager.AppManager;

public class SetActivity extends PreferenceActivity {

	SharedPreferences mPreferences;
	Preference account;
	Preference myinfo;
	Preference cache;
	Preference feedback;
	Preference update;
	Preference about;

	Preference saveImagePath;

	CheckBoxPreference httpslogin;
	CheckBoxPreference loadimage;
	CheckBoxPreference scroll;
	CheckBoxPreference voice;
	CheckBoxPreference checkup;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// 添加Activity到堆栈
		AppManager.getAppManager().addActivity(this);
		
		// 设置显示Preferences
		addPreferencesFromResource(R.xml.preferences);
		// 获得SharedPreferences
		mPreferences = PreferenceManager.getDefaultSharedPreferences(this);

		ListView localListView = getListView();
		localListView.setBackgroundColor(0);
		localListView.setCacheColorHint(0);
		((ViewGroup) localListView.getParent()).removeView(localListView);
		ViewGroup localViewGroup = (ViewGroup) getLayoutInflater().inflate(
				R.layout.setting, null);
		((ViewGroup) localViewGroup.findViewById(R.id.setting_content))
				.addView(localListView, -1, -1);
		setContentView(localViewGroup);

		final AppContext ac = (AppContext) getApplication();

		

		// 我的资料
		myinfo = (Preference) findPreference("myinfo");
		myinfo.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
			public boolean onPreferenceClick(Preference preference) {
				UIHelper.showUserInfo(SetActivity.this);
				return true;
			}
		});


		// 加载图片loadimage
		loadimage = (CheckBoxPreference) findPreference("loadimage");
//		loadimage.setChecked(ac.isLoadImage());
		loadimage.setChecked(false);
		loadimage.setSummary("页面加载图片 (默认在WIFI网络下加载图片)");
		/*if (ac.isLoadImage()) {
			loadimage.setSummary("页面加载图片 (默认在WIFI网络下加载图片)");
		} else {
			loadimage.setSummary("页面不加载图片 (默认在WIFI网络下加载图片)");
		}*/
		loadimage.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
					public boolean onPreferenceClick(Preference preference) {
						
						if (loadimage.isChecked()) {
							loadimage.setSummary("页面加载图片 (默认在WIFI网络下加载图片)");
						} else {
							loadimage.setSummary("页面不加载图片 (默认在WIFI网络下加载图片)");
						}
						return true;
					}
				});

		// 左右滑动
		scroll = (CheckBoxPreference) findPreference("scroll");
		scroll.setChecked(ac.isScroll());
		if(ac.isScroll()){
			scroll.setSummary("已启用左右滑动");
			
		}
		scroll.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
			public boolean onPreferenceClick(Preference preference) {
				ac.setConfigScroll(scroll.isChecked());
				if (scroll.isChecked()) {
					scroll.setSummary("已启用左右滑动");
				} else {
					scroll.setSummary("已关闭左右滑动");
				}
				return true;
			}
		});

		

		// 计算缓存大小
		long fileSize = 0;
		String cacheSize = "0KB";
		File filesDir = getFilesDir();
		File cacheDir = getCacheDir();

		fileSize += FileUtils.getDirSize(filesDir);
		fileSize += FileUtils.getDirSize(cacheDir);
		// 2.2版本才有将应用缓存转移到sd卡的功能
		if (AppContext.isMethodsCompat(android.os.Build.VERSION_CODES.FROYO)) {
			File externalCacheDir = MethodsCompat.getExternalCacheDir(this);
			fileSize += FileUtils.getDirSize(externalCacheDir);
		}
		if (fileSize > 0)
			cacheSize = FileUtils.formatFileSize(fileSize);

		// 清除缓存
		cache = (Preference) findPreference("cache");
		cache.setSummary(cacheSize);
		cache.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
			public boolean onPreferenceClick(Preference preference) {
//				UIHelper.clearAppCache(SetActivity.this);
				/*AppContext haoDroid = ((AppContext)activity.getApplicationContext());
			    imageLoader=haoDroid.getImageLoader();*/
				ac.getImageLoader().clearCache();
				cache.setSummary("0KB");
				return true;
			}
		});

		

		// 关于我们
		about = (Preference) findPreference("about");
		about.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
			public boolean onPreferenceClick(Preference preference) {
				UIHelper.showAbout(SetActivity.this);
				return true;
			}
		});

	}

	public void back(View paramView) {
		finish();
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);

		if (intent.getBooleanExtra("LOGIN", false)) {
			account.setTitle(R.string.main_menu_logout);
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		// 结束Activity&从堆栈中移除
		AppManager.getAppManager().finishActivity(this);
	}
}
