package com.zwh.app.bean;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.zwh.app.adapter.ListViewHomeLineAdapter;

public class StatusPaser {
	private static String hasnext;
	
	public static String getStatuses(String json,ListViewHomeLineAdapter lvHomeLineAdapter,String pageflag){
		
		try {
			JSONObject	myjson=new JSONObject(json);
			JSONObject mydata=myjson.getJSONObject("data");
			hasnext=mydata.getString("hasnext");
			JSONArray info=mydata.getJSONArray("info");
			for(int i=0;i<info.length();i++){
			
				JSONObject object=info.getJSONObject(i);
				
				
				Map<String,Object>map =new HashMap<String,Object>();
				map.put("head", object.getString("head"));
				map.put("nick", object.getString("nick"));
				String origtext=object.getString("origtext");
				if(origtext.length()==0||origtext==null){
					JSONObject source=object.getJSONObject("source");
					origtext=source.getString("origtext");
				}
				map.put("origtext", origtext);
				map.put("timestamp", object.getString("timestamp"));
				if(pageflag.equals("1")||pageflag.equals("0")){ //向下翻页
					lvHomeLineAdapter.addItem(map);
				}else if(pageflag.equals("2")){
					lvHomeLineAdapter.addFreshItem(map,i);
				}
				
				
			}
//			return "数组的长度为:"+info.length();
	
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return hasnext;
	}

}
