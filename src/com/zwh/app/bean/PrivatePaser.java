package com.zwh.app.bean;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.zwh.app.adapter.ListViewMentionsTimelineAdapter;
import com.zwh.app.adapter.ListViewPrivateAdapter;

public class PrivatePaser {
	private static String hasnext;

	public static String getStatuses(String json,ListViewPrivateAdapter lvHomeLineAdapter){
		
			try {
				JSONObject	myjson=new JSONObject(json);
				JSONObject mydata=myjson.getJSONObject("data");
//				hasnext : 0-表示还有微博可拉取，1-已拉取完毕,
				hasnext=mydata.getString("hasnext");
				JSONArray info=mydata.getJSONArray("info");
				for(int i=0;i<info.length();i++){
					Statuses status=new Statuses();
					
					JSONObject object=info.getJSONObject(i);
					status.setHead(object.getString("head"));
					status.setNick(object.getString("nick"));
					status.setOrigtext(object.getString("origtext"));
					
					Map<String,Object>map =new HashMap<String,Object>();
					map.put("head", object.getString("head"));
					map.put("nick", object.getString("nick"));
					map.put("origtext", object.getString("origtext"));
					map.put("timestamp", object.getString("timestamp"));
					map.put("id",  object.getString("id"));
					lvHomeLineAdapter.addItem(map);
					
				}
//				return "数组的长度为:"+info.length();
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return hasnext;
		}
}
