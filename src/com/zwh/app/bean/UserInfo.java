package com.zwh.app.bean;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * 用户本人信息
 * @author Administrator
 *
 */
public class UserInfo {
	//头像url
	private String head;
	
	private String favnum;//收藏数
	private String idolnum;//收听
	private String fansnum;//听众数
	
	/*birth_day : 出生天,
	birth_month : 出生月,
	birth_year : 出生年,*/
	private String birth_day;
	private String birth_month;
	private String birth_year;
	private String nick;
	private String name;
	
	private String level;
	private String location;
	private static UserInfo userinfo=new UserInfo();
	public static UserInfo getUser(String json){
		
		try {
			JSONObject	myjson=new JSONObject(json);
			JSONObject mydata=myjson.getJSONObject("data");
			
			userinfo.setHead(mydata.getString("head"));
			
			userinfo.setFavnum(mydata.getString("favnum"));
			userinfo.setIdolnum(mydata.getString("idolnum"));
			userinfo.setFansnum(mydata.getString("fansnum"));
			
			userinfo.setBirth_year(mydata.getString("birth_year"));
			userinfo.setBirth_month(mydata.getString("birth_month"));
			userinfo.setBirth_day(mydata.getString("birth_day"));
			userinfo.setNick(mydata.getString("nick"));
			userinfo.setLevel(mydata.getString("level"));
			userinfo.setLocation(mydata.getString("location"));
			userinfo.setName(mydata.getString("name"));
			
			
		
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null ;
		}
		
		return userinfo;
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	public String getHead() {
		return head;
	}
	public void setHead(String head) {
		this.head = head;
	}
	public String getFavnum() {
		return favnum;
	}
	public void setFavnum(String favnum) {
		this.favnum = favnum;
	}
	public String getIdolnum() {
		return idolnum;
	}
	public void setIdolnum(String idolnum) {
		this.idolnum = idolnum;
	}
	public String getFansnum() {
		return fansnum;
	}
	public void setFansnum(String fansnum) {
		this.fansnum = fansnum;
	}
	public String getBirth_day() {
		return birth_day;
	}
	public void setBirth_day(String birth_day) {
		this.birth_day = birth_day;
	}
	public String getBirth_month() {
		return birth_month;
	}
	public void setBirth_month(String birth_month) {
		this.birth_month = birth_month;
	}
	public String getBirth_year() {
		return birth_year;
	}
	public void setBirth_year(String birth_year) {
		this.birth_year = birth_year;
	}
	public String getNick() {
		return nick;
	}
	public void setNick(String nick) {
		this.nick = nick;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	
	

}
