#android-hao

项目已停止维护！！！


2013年底，完全采用开源中国客户端的UI，修改了个腾讯微博客户端，
需要熟悉腾讯开放平台API的同学可以看看，算是对社区的回馈吧！


相关学习资料请参考我的博客：
http://my.oschina.net/zhongwenhao/blog?catalog=351790

以实现的功能有：
1、Oauth2.0授权登录
2、发表微博（支持QQ表情）
3、浏览微博（支持自动下来刷新，图片的异步加载）
4、查看关于我
5、查看私信 
6、系统设置


采用的开源软件有：
Lazylist https://github.com/thest1/LazyList
PullToRefresh https://github.com/johannilsson/android-pulltorefresh
GreenDroid http://greendroid.cyrilmottier.com/
项目界面相关开源项目（开源中国android客户端GPL协议）官方网址如下：
http://www.oschina.net/


